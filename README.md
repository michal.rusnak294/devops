# Welcome to DevOps World


## Schedule
6.2.2023 - 8.2.2023


### DAY1
**Docker**
  - review Linux knowledge
  - review Docker knowledge
  - build Docker image using Dockerfile
  - managing Docker containers
  - get to know [dockerhub.com](dockerhub.com) for publishing Docker Images
  - push own image into docker.hub
  - docker compose

_LABS:_
- create web server hosting code from https://github.com/ritaly/HTML-CSS-CV-demo
- write Dockerfile and build own Docker image
- store and manage files related to build procedure in gitlab
- push Docker Image into docker registry
- manage dockerized app using docker compose

**Welcome to Cloud world**
  - intro to Cloud
  - gettting familiar with the AWS cloud provider
  - deploy resources within AWS
    - VPC
    - NATGW
    - IGW
    - Subnets
    - Security Groups
    - EC2
    - S3
  - focus on deploying EC2 machines with postinstall configuration
  
_LABS:_
- deploy EC2 with your application runnig -> mycv
- deploy EC2 with k3s installed
  - Ubuntu 22.04
  - 2xCPU 2GB

<br></br>

### DAY2
***Leftovers from DAY1*
- awscli

_LABS:_
- create networking stack within AWS


**Automate deployments via Terraform**
  - intro to the IaC using [Terraform](https://www.terraform.io/)
  - demo how deploy resources in AWS using Terraform

_LABS:_
- install terrafom
- get familiar with terraform commands 
  - fmt
  - validate
  - plan
  - apply
  - destroy
- review terraform docu
- deploy resources using terraform

**Open Telekom Cloud aka OTC**
  - gettting familiar with the OTC cloud provider
  - deploy resources within OTc
    - VPC
    - NATGW
    - IGW
    - Subnets
    - Security Groups
    - ECS
  - focus on deploying ECS machines with postinstall configuration

_LABS:_
- create networking stack within OTC
- deploy ECS with running webserver
- deploy ECS with k3s installed

### DAY3
**Let's automate CI/CD**
  - intro to GitLab CICD / gitlab.com
  - demo how to manage directories/projects within gitlab
  - building CICD pipelines using Gitlab CICD
  - automate deployments in AWS using GitlabCICD & Terraform

_LABS:_
- create directory/projects structure in gitlab
- intro to Gitlab CICD pipelines
- build own CICD pipeline for golang development
- create,test,troubleshoot Gitlab CICD for automated deployments in AWS using Terraform
- deploy 3-node k8s cluster using Gitlab CICD pipeline

**Final Exam**

### Bonus
**GitOps using FLUX CD**
  - intro to GitOps
  - setup Flux CD
  - deploy app via Flux CD

**Visit Server room**

### Useful Links
- 
