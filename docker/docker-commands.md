# Docker commands

```script
# get Docker version
docker version

# search for image within Docker registry
docker search nginx

# download image
docker pull nginx

# remove image from local system
docker rmi nginx:latest

# start container from a specific image
docker run -dit nginx
docker run -dit --name -p 8000:80 rocky1 petyb/rocky:1

# display running containers
docker ps

# display all containers including exited ones
docker ps -

# start and stop container
docker stop
docker start

# build image
docker build -t petyb/rocky:1 .

# login to dockerhub.com
docker login

# push image to docker registry
docker push petyb/rocky:1

# get container logs
docker logs 
```
